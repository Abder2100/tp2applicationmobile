package fr.uavignon.ceri.tp2;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    private DetailViewModel detailViewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        detailViewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        textTitle = (EditText) getView().findViewById(R.id.nameBook);
        textAuthors = (EditText) getView().findViewById(R.id.editAuthors);
        textYear = (EditText) getView().findViewById(R.id.editYear);
        textGenres = (EditText) getView().findViewById(R.id.editGenres);
        textPublisher = (EditText) getView().findViewById(R.id.editPublisher);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        long bookId = args.getBookNum();
        detailViewModel.setBook(bookId);
        observerSetup();
        listenerSetup();

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }

    private void observerSetup() {
        detailViewModel.getSelectedBook().observe(getViewLifecycleOwner(),
                new Observer<Book>() {
                    @Override
                    public void onChanged(Book book) {
                        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
                        long bookId = args.getBookNum();
                        if (bookId != -1) {
                            detailViewModel.getSelectedBook().observe(getViewLifecycleOwner(),
                                    new Observer<Book>() {
                                        @Override
                                        public void onChanged(@Nullable final Book book) {
                                            textTitle.setText(book.getTitle());
                                            textAuthors.setText(book.getAuthors());
                                            textYear.setText(book.getYear());
                                            textGenres.setText(book.getGenres());
                                            textPublisher.setText(book.getPublisher());
                                        }
                                    });
                        } else {
                            // cliner les champs
                            textTitle.setText("");
                            textAuthors.setText("");
                            textYear.setText("");
                            textGenres.setText("");
                            textPublisher.setText("");

                        }

                    }
                });
    }

    private void listenerSetup() {
        Button updateButton = (Button) getView().findViewById(R.id.buttonUpdate);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = getContext();
                CharSequence text = "updated";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();


                String name = textTitle.getText().toString();
                String autor = textAuthors.getText().toString();
                String year = textYear.getText().toString();
                String genre = textGenres.getText().toString();
                String editeur = textPublisher.getText().toString();
                if (!name.equals("") && !autor.equals("") && !year.equals("") && !genre.equals("") && !editeur.equals("")) {
                    Book book = new Book(name, autor, year, genre, editeur);

                    if (detailViewModel.getSelectedBook().getValue() != null)
                        book.setId(detailViewModel.getSelectedBook().getValue().getId());

                    detailViewModel.insertOrUpdateBook(book);
                    // cliner les champs
                    textTitle.setText("");
                    textAuthors.setText("");
                    textYear.setText("");
                    textGenres.setText("");
                    textPublisher.setText("");

                } else {
                    Snackbar snackbar = Snackbar
                            .make(view, "veuillez remplir tous les champs SVP !", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
    }


}