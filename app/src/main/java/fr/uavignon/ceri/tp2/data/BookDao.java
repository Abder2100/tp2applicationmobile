package fr.uavignon.ceri.tp2.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
@Dao
public interface BookDao {
    @Update
    void updateBook(Book book);
    @Insert
    void insertBook(Book book);
    @Query("select * from books where bookId = :id")
    Book getBook(long id);
    @Query("delete from books where bookId = :id")
    void deleteBook(long id);
    @Query("select * from books")
    LiveData<List<Book>> getAllBooks();
    @Query("delete from books")
    void deleteAllBooks();
}
