package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {

    private LiveData<List<Book>> allBooks;
    private MutableLiveData<Book> selectedBook;
    private BookRepository repository;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllBooks();
        selectedBook = repository.getSelectedBook();
    }
    MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }
    LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }
    public void insertBook(Book book) {
        repository.insertBook(book);
    }
    public void getBook(long id) {
        repository.getBook(id);
    }
    public void deleteBook(long id) {
        repository.deleteBook(id);
    }
    public void updateBook(Book book) {
        repository.updateBook(book);
    }

    public void setBook(long bookId) {
        repository.getBook(bookId);
        selectedBook = repository.getSelectedBook();

    }
}
