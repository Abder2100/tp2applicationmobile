package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.concurrent.*;

import java.util.List;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;


public class BookRepository {
    private MutableLiveData<Book> selectedBook = new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;
    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getInstance(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }

    public void insertBook(Book newbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newbook);
        });
    }

    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
    }

    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }

    public void getBook(long id) {
        Future<Book> book = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(book.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
