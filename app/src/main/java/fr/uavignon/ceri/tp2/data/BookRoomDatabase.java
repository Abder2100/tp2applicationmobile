package fr.uavignon.ceri.tp2.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Book.class}, version = 1)
public abstract class BookRoomDatabase extends RoomDatabase {
    public abstract BookDao bookDao();

    private static volatile BookRoomDatabase instance;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static BookRoomDatabase getInstance(final Context context) {

        if (instance == null) {
            synchronized (BookRoomDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            BookRoomDatabase.class,
                            "books").addCallback(sRoomDatabaseCallback).build();
                }
            }
        }
        return instance;
    }

    private static Callback sRoomDatabaseCallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            databaseWriteExecutor.execute(() -> {
                // Populate the database in the background.
                BookDao dao = instance.bookDao();
                dao.deleteAllBooks();

                for (Book newBook : Book.books)
                    dao.insertBook(newBook);

            });
        }
    };
}
